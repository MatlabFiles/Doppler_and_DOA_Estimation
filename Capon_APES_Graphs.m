function Capon_APES_Graphs(SNR,BetaT,wsT,wdT,wsI,X,Rz,INR,nT,nR,gamma,N,Mc,NnS,NnD)
wwD = 2*pi*(-0.5:1/(NnD):(0.5-1/(NnD)));
wwS = 2*pi*(-0.5:1/(NnS):(0.5-1/(NnS)));
I=length(wsI);
T=length(wsT);

Rxx=X*X'/N;
X3D=reshape(X,nT,1,N);


yT=0;
for i=1:T
    aT=exp(-1j*gamma*wsT(i)*(0:nT-1)');
    aR=exp( 1j*wsT(i)*(0:nR-1)');
    yT=yT+BetaT(i)*(ones(nR,1)*exp(1j*wdT(i)*(0:N-1))).*(aR*aT'*X);
end

yI=0;
for i=1:I
    aT=exp(-1j*gamma*wsI(i)*(0:nT-1)');
    aR=exp( 1j*wsI(i).*(0:nR-1)');
    yI=yI+exp(1j*2*pi*rand)*aR*aT'*X;
end

mat=zeros(nR-1+gamma*(nT-1)+1,nT,N);
for n=1:N
    for p=1:nT
        mat(gamma*(p-1)+(1:nR),p,n)=1;
    end
end
mat=logical(mat);

P_Theta=zeros(NnD,NnS);
for f=1:NnS
    at=exp(-1j*gamma*wwS(f)*(0:nT-1)');
    P_Theta(:,f)=ones(NnD,1)*real(at'*Rxx*at);
end


for k=1:Mc    
    sigma_z=sqrt(trace(yT*yT'/N)/10^(SNR/10));
    z=sigma_z*include.GenerateGaussianNoise(nR,N,Rz);
    
    if I >0
        sigma_i=sqrt(10^(INR/10)*trace(z*z'/N)/trace(yI*yI'/N));
        yI=sigma_i*yI;
    end
    
    y=yT+yI+z;      
    Ry=y*y'/N;
    RyInv=inv(Ry);
    

    r= Ry\y;
    Mr_in=zeros(nR-1+gamma*(nT-1)+1,nT,N);
    Mr_in(mat)=kron(ones(nT,1),r);
    XR_in=squeeze(include.mmat(Mr_in,conj(X3D)));        

    Ay=fftshift(abs(fft2(XR_in.',NnD,NnS)/N).^2);
    
    
    U=chol(RyInv);
    r = U*y;
    XR_u=zeros(gamma*(nT-1)+1,N,nR);
    X_G=zeros(gamma*(nT-1)+1,N);
    X_G(gamma*(0:nT-1)+1,:)=X;
    for m=1:nR
        XR_u(:,:,m) = (ones(gamma*(nT-1)+1,1)*r(m,:)).*conj(X_G);
    end
    XR_u=permute(XR_u,[2,1,3]);
    Res_v=fft2(XR_u,NnD,NnS)/N;
    Res=fftshift(sum(abs(Res_v).^2,3));
        
        
    arH_RyInv_ar=zeros(NnD,NnS);
    for f=1:NnS
        ar=exp( 1j*wwS(f)*(0:nR-1)');
        arH_RyInv_ar(:,f)=ones(NnD,1)*real(ar'*RyInv*ar);
    end
    

    Capon=Ay./(P_Theta.*arH_RyInv_ar).^2;
    Apes_conventional=Ay./(Ay+(P_Theta-Res).*arH_RyInv_ar).^2;
    Part_Apes_proposed=Ay./(P_Theta-Res);
    Apes_proposed=arH_RyInv_ar+ Ay./(P_Theta-Res); 
end

figure('units','normalized','position',[0 0.52 0.3 0.4]);
clf;imagesc(wwS,wwD,Capon./max(max(Capon))); colorbar
colormap(flipud(colormap(hot)))
xlabel('$\bar \omega$','Interpreter','latex')
ylabel('$\omega$','Interpreter','latex')
title('Capon')
set(gcf,'Color','w')
hold on;plot3(wsT,wdT,ones(T,1),'bo','MarkerSize',20)
hold on;plot3(wsI,zeros(I,1),ones(I,1),'go','MarkerSize',20)

figure('units','normalized','position',[0 0.04 0.3 0.4]);
clf;imagesc(wwS,wwD,Apes_conventional./max(max(Apes_conventional))); colorbar
colormap(flipud(colormap(hot)))
xlabel('$\bar \omega$','Interpreter','latex')
ylabel('$\omega$','Interpreter','latex')
title('Conventional APES')
set(gcf,'Color','w')
hold on;plot3(wsT,wdT,ones(T,1),'bo','MarkerSize',20)
hold on;plot3(wsI,zeros(I,1),ones(I,1),'go','MarkerSize',20)


figure('units','normalized','position',[0.3 0.52 0.3 0.4]);
clf;imagesc(wwS,wwD,Part_Apes_proposed./max(max(Part_Apes_proposed))); colorbar
colormap(flipud(colormap(hot)))
xlabel('$\bar \omega$','Interpreter','latex')
ylabel('$\omega$','Interpreter','latex')
title('One term of the Proposed APES')
set(gcf,'Color','w')
hold on;plot3(wsT,wdT,ones(T,1),'bo','MarkerSize',20)
hold on;plot3(wsI,zeros(I,1),ones(I,1),'go','MarkerSize',20)

figure('units','normalized','position',[0.3 0.04 0.3 0.4]);
clf;imagesc(wwS,wwD,Apes_proposed./max(max(Apes_proposed))); colorbar
colormap(flipud(colormap(hot)))
xlabel('$\bar \omega$','Interpreter','latex')
ylabel('$\omega$','Interpreter','latex')
title('Proposed APES')
set(gcf,'Color','w')
hold on;plot3(wsT,wdT,ones(T,1),'bo','MarkerSize',20)
hold on;plot3(wsI,zeros(I,1),ones(I,1),'go','MarkerSize',20)

