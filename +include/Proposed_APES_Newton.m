function [wdh,wsh,l_new,iter,Vect]=Proposed_APES_Newton(wdh,wsh,X,Mr,r,nT,nR,gamma,N,wD,wS)
        gain=1;
        gain_wsh=1;
        gain_wdh=1;
        iter=1;
        Rxx=X*X'/N;
        grad=1;
        Vect=[];
        TransX=reshape(X,1,nT,N);
        TransXHermMr=squeeze(include.mmat(TransX,permute(conj(Mr),[2 1 3])));
        Conjr=reshape(conj(r),nR,1,N);
        ConjrTransX=include.mmat(Conjr,TransX);
%         while ((gain_wsh>1e-10)||(gain_wdh>1e-10)) && (iter<500)
        while (gain>1e-4) && (iter<100)
            [Eval_P,Grad_P,Hess_P]=include.Eval_and_Grad_P(wsh(iter),Rxx,nT,gamma);

            V_0= exp(1j*wdh(iter)*(0:N-1));
            W_0= exp(1j*wsh(iter)*(0:(nR-1+gamma*(nT-1)))')*V_0;
            W_D = 1j * diag(0:(nR-1+gamma*(nT-1)))*W_0;
            W_DD= 1j * diag(0:(nR-1+gamma*(nT-1)))*W_D;
            Mat_1= sum( W_0  .* TransXHermMr)/N;
            Mat_2= sum( W_D  .* TransXHermMr)/N;
            Mat_3= sum( W_DD .* TransXHermMr)/N;

            PJ0 = sum(Mat_1);
            PJwD1 = Mat_1*(1j*(0:N-1)');
            PJwD2 = Mat_1*(1j*(0:N-1)').^2;
            PJwS1 = sum(Mat_2);
            PJwS2 = sum(Mat_3);
            PJwSwD = Mat_2*(1j*(0:N-1)');

            Eval_S_2 =  PJ0'*PJ0;
            Grad_wd_S_2 = 2*real(PJwD1'*PJ0);
            Grad_ws_S_2 = 2*real(PJwS1'*PJ0);
            Hess_wd_S_2 = 2*(PJwD1'*PJwD1+real(PJwD2'*PJ0));
            Hess_ws_S_2 = 2*(PJwS1'*PJwS1+real(PJwS2'*PJ0));
            Hess_wd_ws_S_2 = 2*real( PJwD1' * PJwS1 + PJwSwD' * PJ0 );

            O_0= exp(1j*wsh(iter)*gamma*(0:(nT-1))') * V_0;
            O_D = 1j *gamma* diag(0:(nT-1))*O_0;
            O_DD= 1j *gamma* diag(0:(nT-1))*O_D;
            Mat_11= squeeze(include.mmat(ConjrTransX,reshape(O_0 ,nT,1,N)))/N;
            Mat_12= squeeze(include.mmat(ConjrTransX,reshape(O_D ,nT,1,N)))/N;
            Mat_13= squeeze(include.mmat(ConjrTransX,reshape(O_DD,nT,1,N)))/N;

            PJ_0 = sum(Mat_11,2);
            PJ_wD1 = Mat_11*(1j*(0:N-1)');
            PJ_wD2 = Mat_11*(1j*(0:N-1)').^2;
            PJ_wS1 = sum(Mat_12,2);
            PJ_wS2 = sum(Mat_13,2);
            PJ_wSwD = sum(ones(nR,1)*(1j*(0:N-1)).*Mat_12,2);

            Eval_G_2= PJ_0'*PJ_0;
            Grad_wd_G_2 = 2*real(PJ_wD1'*PJ_0);
            Grad_ws_G_2 = 2*real(PJ_wS1'*PJ_0);
            Hess_wd_G_2 = 2*(PJ_wD1'*PJ_wD1+real(PJ_wD2'*PJ_0));
            Hess_ws_G_2 = 2*(PJ_wS1'*PJ_wS1+real(PJ_wS2'*PJ_0));
            Hess_wd_ws_G_2 = 2*real( PJ_wD1'*PJ_wS1 + PJ_wSwD'*PJ_0);


            Eval_H = Eval_P - Eval_G_2;
            Grad_wd_H = - Grad_wd_G_2;
            Hess_wd_H = - Hess_wd_G_2;
            Grad_ws_H = Grad_P-Grad_ws_G_2;
            Hess_ws_H = Hess_P-Hess_ws_G_2;
            Hess_wd_ws_H = - Hess_wd_ws_G_2;

            if (iter==1)
                l_new(1)=Eval_S_2/Eval_H;
            end

            G(1,1) = Grad_wd_S_2 / Eval_H - Eval_S_2 * Grad_wd_H / Eval_H^2;
            G(2,1) = Grad_ws_S_2 / Eval_H - Eval_S_2 * Grad_ws_H / Eval_H^2;
            H(1,1) = Hess_wd_S_2 / Eval_H - 2 * Grad_wd_S_2 * Grad_wd_H / Eval_H^2 - Eval_S_2 * Hess_wd_H / Eval_H^2 + 2 * Eval_S_2 * Grad_wd_H^2 / Eval_H^3;
            H(2,2) = Hess_ws_S_2 / Eval_H - 2 * Grad_ws_S_2 * Grad_ws_H / Eval_H^2 - Eval_S_2 * Hess_ws_H / Eval_H^2 + 2 * Eval_S_2 * Grad_ws_H^2 / Eval_H^3;
            H(1,2) = Hess_wd_ws_S_2 / Eval_H - Grad_wd_S_2 * Grad_ws_H / Eval_H^2 - Grad_ws_S_2 * Grad_wd_H / Eval_H^2 - Eval_S_2 * Hess_wd_ws_H /  Eval_H^2 + 2 * Eval_S_2 * Grad_ws_H * Grad_wd_H / Eval_H^3;
            H(2,1) = H(1,2);
            newtonDir=- H\G;
            if  ( G'*newtonDir<0)
                grad=G;
            else
                grad=newtonDir;
            end

            if (norm(grad)>1e-2)
                grad=grad*1e-2/norm(grad);
            end

            tt = [wdh(iter);wsh(iter)] + grad;


            iter=iter+1;
            wdh(iter) = tt(1);
            wsh(iter) = tt(2);

            V_0= exp(1j*wdh(iter)*(0:N-1));
            W_0= exp(1j*wsh(iter)*(0:(nR-1+gamma*(nT-1)))')*V_0;
            Mat_1= sum( W_0  .* TransXHermMr)/N;
            PJ0 = sum(Mat_1);
            Eval_S_2 =  PJ0'*PJ0;
            O_0= exp(1j*wsh(iter)*gamma*(0:(nT-1))') * V_0;
            Mat_11= squeeze(include.mmat(ConjrTransX,reshape(O_0 ,nT,1,N)))/N;
            PJ_0 = sum(Mat_11,2);
            Eval_G_2= PJ_0'*PJ_0;


            [Eval_P]   =include.Eval_and_Grad_P(wsh(iter),Rxx,nT,gamma);

            l_new(iter) =  Eval_S_2/(Eval_P-Eval_G_2);
            t=1;a=0.4; b=0.5;
            Vect(iter)=1;
            while (l_new(iter)<l_new(iter-1)+a*t*(G'*grad)) && (t>=1e-5)
                Vect(iter)=Vect(iter)+1;
                t=t*b;
                tt = [wdh(iter-1);wsh(iter-1)] + t*grad;
                wdh(iter) = tt(1);
                wsh(iter) = tt(2);
                V_0= exp(1j*wdh(iter)*(0:N-1));
                W_0= exp(1j*wsh(iter)*(0:(nR-1+gamma*(nT-1)))')*V_0;
                Mat_1= sum( W_0  .* TransXHermMr)/N;
                PJ0 = sum(Mat_1);
                Eval_S_2 =  PJ0'*PJ0;
                O_0= exp(1j*wsh(iter)*gamma*(0:(nT-1))') * V_0;
                Mat_11= squeeze(include.mmat(ConjrTransX,reshape(O_0 ,nT,1,N)))/N;
                PJ_0 = sum(Mat_11,2);
                Eval_G_2= PJ_0'*PJ_0;

                [Eval_P]   =include.Eval_and_Grad_P(wsh(iter),Rxx,nT,gamma);

                l_new(iter) =  Eval_S_2/(Eval_P-Eval_G_2);
            end
            if (t<1e-5)
                break
            end
            gain=abs(l_new(end-1)-l_new(end));
            gain_wsh=abs(wsh(end-1)-wsh(end));
            gain_wdh=abs(wdh(end-1)-wdh(end));
        end
%         figure(11);hold on;plot(l_new')
%         figure(12);plot(1:iter,wD*ones(1,iter),'b');hold on;plot(wdh')
%         figure(13);plot(1:iter,wS*ones(1,iter),'b');hold on;plot(wsh')
        wsh=wsh(end);
        wdh=wdh(end);
%         keyboard
% iter
% gain
