function [eval,grad,hess]=Eval_and_Grad_aRa(wsT,R,nR)
aR=exp( 1j*wsT.*(0:nR-1)');
Vec=zeros(nR,1);
for i=0:nR-1 
    Vec(i+1)=sum(diag(R,i));
end
Vec(1)=Vec(1)/2;
eval= 2*real(Vec.'*aR);
grad=-2*imag(Vec.'*diag(0:nR-1)*aR);
hess=-2*real(Vec.'*diag(0:nR-1)*diag(0:nR-1)*aR);