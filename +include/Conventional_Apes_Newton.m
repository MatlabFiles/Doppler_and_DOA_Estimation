function [wdh,wsh,l_new,iter]=Conventional_Apes_Newton(wdh,wsh,X,Mr,Ry,r,nT,nR,gamma,N,wD,wS)
        gain=1;
        gain_wsh=1;
        gain_wdh=1;
        iter=1;
        Rxx=X*X'/N;
        invRy=inv(Ry);

        ConjX=reshape(conj(X),nT,1,N);
        MrConjX=squeeze(include.mmat(Mr,ConjX));
        r3d=reshape(r,1,nR,N);
        ConjXr3d=include.mmat(ConjX,r3d);
%         while ((gain_wsh>1e-6)||(gain_wdh>1e-6)) && (iter<200)
        while (gain>1e-4) && (iter<100)
            [Eval_ar_Ryinv_ar,Grad_ar_Ryinv_ar,Hess_ar_Ryinv_ar]=include.Eval_and_Grad_aRa(wsh(iter),invRy,nR);
            [Eval_P,Grad_P,Hess_P]                    =include.Eval_and_Grad_P(wsh(iter),Rxx,nT,gamma);

            V_0= exp(-1j*wdh(iter)*(0:N-1));
            W_0= exp(-1j*wsh(iter)*(0:(nR-1+gamma*(nT-1)))') * V_0;
            W_D= diag(0:(nR-1+gamma*(nT-1)))*W_0;
            W_DD= diag(0:(nR-1+gamma*(nT-1)))*W_D;
            Mat_1= sum( W_0  .* MrConjX);
            Mat_2= sum( W_D  .* MrConjX);
            Mat_3= sum( W_DD .* MrConjX);

            PJfD1 = sum(Mat_1);
            PJfD2 = Mat_1*(0:N-1)';
            PJfD3 = Mat_1*(0:N-1)'.^2;
            PJTh1 = sum(Mat_2);
            PJTh2 = sum(Mat_3);
            PJTh3 = Mat_2*(0:N-1)';

            Eval_S_2 =  PJfD1'*PJfD1/N^2;
            Grad_wd_S_2 = -2/N^2*imag(PJfD2'*PJfD1);
            Hess_wd_S_2 = 2/N^2*(PJfD2'*PJfD2-real(PJfD3'*PJfD1));
            Grad_ws_S_2 =  -2/N^2*imag(PJTh1'*PJfD1);
            Hess_ws_S_2 =  2/N^2*(PJTh1'*PJTh1-real(PJTh2'*PJfD1));
            Hess_wd_ws_S_2 = 2/N^2*real( PJfD2' * PJTh1 - PJTh3' * PJfD1 );

            O_0= exp(-1j*wsh(iter)*gamma*(0:(nT-1))') * V_0;
            O_D= diag(0:(nT-1))*O_0;
            O_DD= diag(0:(nT-1))*O_D;
            Mat_11= squeeze(include.mmat(reshape(O_0 ,1,nT,N),ConjXr3d));
            Mat_12= squeeze(include.mmat(reshape(O_D ,1,nT,N),ConjXr3d));
            Mat_13= squeeze(include.mmat(reshape(O_DD,1,nT,N),ConjXr3d));

            PJfD21 = sum(Mat_11,2);
            PJfD22 = Mat_11*(0:N-1)';
            PJfD23 = Mat_11*(0:N-1)'.^2;
            PJTh21 = sum(Mat_12,2);
            PJTh22 = sum(Mat_13,2);
            PJTh23 = sum(ones(nR,1)*(0:N-1).*Mat_12,2);

            Eval_G_2= PJfD21'*PJfD21/N^2;
            Grad_wd_G_2 = -2/N^2*imag(PJfD22'*PJfD21);
            Hess_wd_G_2 = 2/N^2*(PJfD22'*PJfD22-real(PJfD23'*PJfD21));
            Grad_ws_G_2 = -2*gamma/N^2*imag(PJTh21'*PJfD21);
            Hess_ws_G_2 = 2*gamma^2/N^2*(PJTh21'*PJTh21-real(PJTh22'*PJfD21));
            Hess_wd_ws_G_2 = 2*gamma/N^2*real( PJfD22'*PJTh21 - PJTh23'*PJfD21);


            Eval_H = Eval_S_2+Eval_ar_Ryinv_ar*(Eval_P-Eval_G_2);
            Grad_wd_H = Grad_wd_S_2 - Eval_ar_Ryinv_ar*Grad_wd_G_2;
            Hess_wd_H = Hess_wd_S_2 - Eval_ar_Ryinv_ar*Hess_wd_G_2;
            Grad_ws_H = Grad_ws_S_2 + Grad_ar_Ryinv_ar*(Eval_P-Eval_G_2)+Eval_ar_Ryinv_ar*(Grad_P-Grad_ws_G_2);
            Hess_ws_H = Hess_ws_S_2 + Hess_ar_Ryinv_ar * (Eval_P-Eval_G_2) + 2 * Grad_ar_Ryinv_ar * (Grad_P-Grad_ws_G_2) + Eval_ar_Ryinv_ar*(Hess_P-Hess_ws_G_2);
            Hess_wd_ws_H = Hess_wd_ws_S_2 - Grad_ar_Ryinv_ar * Grad_wd_G_2 - Eval_ar_Ryinv_ar * Hess_wd_ws_G_2;


            PJwD = Grad_wd_S_2 / Eval_H^2 - 2 * Eval_S_2 * Grad_wd_H / Eval_H^3;
            PJwS = Grad_ws_S_2 / Eval_H^2 - 2 * Eval_S_2 * Grad_ws_H / Eval_H^3;
            Hess_wd = Hess_wd_S_2 / Eval_H^2 - 4 * Grad_wd_S_2 * Grad_wd_H / Eval_H^3 - 2 * Eval_S_2 * Hess_wd_H / Eval_H^3 + 6 * Eval_S_2 * Grad_wd_H^2 / Eval_H^4;
            Hess_ws = Hess_ws_S_2 / Eval_H^2 - 4 * Grad_ws_S_2 * Grad_ws_H / Eval_H^3 - 2 * Eval_S_2 * Hess_ws_H / Eval_H^3 + 6 * Eval_S_2 * Grad_ws_H^2 / Eval_H^4;
            Hess_wd_ws = Hess_wd_ws_S_2 / Eval_H^2 - 2 * Grad_wd_S_2 * Grad_ws_H / Eval_H^3 - 2 * Grad_ws_S_2 * Grad_wd_H / Eval_H^3 - 2 * Eval_S_2 * Hess_wd_ws_H /  Eval_H^3 + 6 * Eval_S_2 * Grad_ws_H * Grad_wd_H / Eval_H^4;

            if (iter==1)
                l_new(1)=Eval_S_2/Eval_H^2;
            end

            grad=[Hess_wd Hess_wd_ws;Hess_wd_ws Hess_ws]\ [PJwD;PJwS];
            if (norm(grad)>1e-3)
                grad= grad/(500*norm(grad));
            end
            tt = [wdh(iter);wsh(iter)] - grad;

            iter=iter+1;
            wdh(iter) = tt(1);
            wsh(iter) = tt(2);

            V_0= exp(-1j*wdh(iter)*(0:N-1));
            W_0= exp(-1j*wsh(iter)*(0:(nR-1+gamma*(nT-1)))') * V_0;
            Mat_1= sum( W_0  .* MrConjX);
            PJfD1 = sum(Mat_1);
            Eval_S_2 =  PJfD1'*PJfD1/N^2;
            O_0= exp(-1j*wsh(iter)*gamma*(0:(nT-1))') * V_0;
            Mat_11= squeeze(include.mmat(reshape(O_0 ,1,nT,N),ConjXr3d));
            PJfD21 = sum(Mat_11,2);
            Eval_G_2= PJfD21'*PJfD21/N^2;

            [Eval_ar_Ryinv_ar]=include.Eval_and_Grad_aRa(wsh(iter),invRy,nR);
            [Eval_P]   =include.Eval_and_Grad_P(wsh(iter),Rxx,nT,gamma);

            l_new(iter)=Eval_S_2/(Eval_S_2+Eval_ar_Ryinv_ar*(Eval_P-Eval_G_2))^2;

            t=1;a=0.4; b=0.4;
            while l_new(iter) <l_new(iter-1)+a*t*(grad'*grad)
                t=t*b;
                tt = [wdh(iter-1);wsh(iter-1)] + t*grad;
                wdh(iter) = tt(1);
                wsh(iter) = tt(2);

                V_0= exp(-1j*wdh(iter)*(0:N-1));
                W_0= exp(-1j*wsh(iter)*(0:(nR-1+gamma*(nT-1)))') * V_0;
                Mat_1= sum( W_0  .* MrConjX);
                PJfD1 = sum(Mat_1);
                Eval_S_2 =  PJfD1'*PJfD1/N^2;
                O_0= exp(-1j*wsh(iter)*gamma*(0:(nT-1))') * V_0;
                Mat_11= squeeze(include.mmat(reshape(O_0 ,1,nT,N),ConjXr3d));
                PJfD21 = sum(Mat_11,2);
                Eval_G_2= PJfD21'*PJfD21/N^2;

                [Eval_ar_Ryinv_ar]=include.Eval_and_Grad_aRa(wsh(iter),invRy,nR);
                [Eval_P]   =include.Eval_and_Grad_P(wsh(iter),Rxx,nT,gamma);
                l_new(iter)=Eval_S_2/(Eval_S_2+Eval_ar_Ryinv_ar*(Eval_P-Eval_G_2))^2;
            end

            gain=l_new(end-1)-l_new(end);
            gain_wsh=abs(wsh(end-1)-wsh(end));
            gain_wdh=abs(wdh(end-1)-wdh(end));
        end
        wsh=wsh(end);
        wdh=wdh(end);
%         keyboard
% iter
% gain
