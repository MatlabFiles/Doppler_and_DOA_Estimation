%% FISHER Matrix
function [CRLB]=CRLB_nR_nT(CRLB_old,nT,nR,gamma,N,wsT,BetaT,SigmaSQ,X,Rin)
Rxx=X*X'/N;

aT=exp(-1j*gamma*wsT.*(0:nT-1)');
aR=exp(1j*wsT.*(0:nR-1)');

q_0=real(aR'/Rin*aR);
q_1=aR'*diag(0:nR-1)/Rin*aR/q_0;
q_2=real(aR'*diag(0:nR-1)/Rin*diag(0:nR-1)*aR)/q_0;

S_00=real(aT'*Rxx*aT);
S_01=aT'*diag(0:nT-1)*Rxx*aT/S_00;
S_02=real(aT'*diag(0:nT-1)*Rxx*diag(0:nT-1)*aT)/S_00;

S_10=0; S_20=0; S_11=0;
for i=1:N
    S_10=S_10+real(1/(N*real(aT'*Rxx*aT))*(i-1)*aT'*(X(:,i)*X(:,i)')*aT);
    S_20=S_20+real(1/(N*real(aT'*Rxx*aT))*(i-1)^2*aT'*(X(:,i)*X(:,i)')*aT);
    S_11=S_11+1/(N*real(aT'*Rxx*aT))*(i-1)*aT'*diag(0:nT-1)*(X(:,i)*X(:,i)')*aT;
end

% F=zeros(4);
% F(1,1)=1;
% F(2,2)=1;
% F(1,3)=-S_10*imag(BetaT);  F(3,1)=F(1,3);
% F(2,3)= S_10*real(BetaT);   F(3,2)=F(2,3);
% F(3,3)=abs(BetaT)^2*S_20;
% F(1,4)=-imag(BetaT*(q_1+gamma*S_01));  F(4,1)=F(1,4);
% F(2,4)= real(BetaT*(q_1+gamma*S_01));  F(4,2)=F(2,4);
% F(3,4)=abs(BetaT)^2*(real(q_1)*S_10+gamma*real(S_11)); F(4,3)=F(3,4);
% F(4,4)=abs(BetaT)^2*(gamma^2*S_02+2*gamma*real(q_1*S_01)+q_2);
% invF=inv(F)

Alpha   = abs(BetaT)^2*(S_20-S_10^2)*(q_2-abs(q_1)^2+gamma^2*(S_02-abs(S_01)^2))-abs(BetaT)^2*gamma^2*(real(S_11)-real(S_01)*S_10)^2;
Delta_1 = S_10^2*(q_2-abs(q_1)^2+gamma^2*(S_02-abs(S_01)^2))+2*gamma*S_10*(S_10*real(S_01)-real(S_11))*(real(q_1)+gamma*real(S_01))+(S_20-S_10^2)*(real(q_1)+gamma*real(S_01))^2;
Delta_2 = 2*(imag(q_1)+gamma*imag(S_01))*(gamma*S_20*real(S_01)-gamma*S_10*real(S_11)+real(q_1)*(S_20-S_10^2));
Delta_3 = (imag(q_1)+gamma*imag(S_01))^2*(S_20-S_10^2);

InvF(1,1)=1+( Delta_1 * imag(BetaT)^2 + Delta_2 * real(BetaT)*imag(BetaT) + Delta_3 * real(BetaT)^2)/Alpha;
InvF(1,2)=1+( Delta_1 * real(BetaT)^2 - Delta_2 * real(BetaT)*imag(BetaT) + Delta_3 * imag(BetaT)^2)/Alpha;
InvF(1,3)=(q_2-abs(q_1)^2+gamma^2*(S_02-abs(S_01)^2))/Alpha;
InvF(1,4)=(S_20-S_10^2)/Alpha;
CRLB=[CRLB_old; SigmaSQ/(2*N*q_0*S_00)*InvF];