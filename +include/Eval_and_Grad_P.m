function [eval,grad,hess]=Eval_and_Grad_P(wsT,R,nT,gamma)
aT=exp(-1j*gamma*wsT.*(0:nT-1)');
Vec=zeros(nT,1);
for i=0:nT-1 
    Vec(i+1)=sum(diag(R,i));
end
Vec(1)=Vec(1)/2;
eval=2*real(Vec.'*aT);
grad=2*gamma*imag(Vec.'*diag(0:nT-1)*aT);
hess=-2*gamma^2*real(Vec.'*diag(0:nT-1)*diag(0:nT-1)*aT);