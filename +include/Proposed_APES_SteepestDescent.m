function [wdh,wsh,l_new,iter,Vect]=Proposed_APES_SteepestDescent(wdh,wsh,X,Mr,r,nT,nR,gamma,N,wD,wS)
        gain=1;
        gain_wsh=1;
        gain_wdh=1;
        grad=1;
        iter=1;
        Vect=[];
        Rxx=X*X'/N;
        ConjX=reshape(conj(X),nT,1,N);
        MrConjX=squeeze(include.mmat(Mr,ConjX));
        r3d=reshape(r,1,nR,N);
        ConjXr3d=include.mmat(ConjX,r3d);
%         while ((gain_wsh>1e-5)||(gain_wdh>1e-5)) && (iter<500)
        while (gain>1e-4) && (iter<100)
            [Eval_P,Grad_P]=include.Eval_and_Grad_P(wsh(iter),Rxx,nT,gamma);

            V_0= exp(-1j*wdh(iter)*(0:N-1));
            W_0= exp(-1j*wsh(iter)*(0:(nR-1+gamma*(nT-1)))') * V_0;
            W_D= diag(0:(nR-1+gamma*(nT-1)))*W_0;
            Mat_1= sum( W_0  .* MrConjX);
            Mat_2= sum( W_D  .* MrConjX);

            PJfD0 = sum(Mat_1);
            PJfD1 = Mat_1*(0:N-1)';
            PJTh1 = sum(Mat_2);

            Eval_S_2 =  PJfD0'*PJfD0/N^2;
            Grad_wd_S_2 = -2/N^2*imag(PJfD1'*PJfD0);
            Grad_ws_S_2 =  -2/N^2*imag(PJTh1'*PJfD0);

            O_0= exp(-1j*wsh(iter)*gamma*(0:(nT-1))') * V_0;
            O_D= diag(0:(nT-1))*O_0;
            Mat_11= squeeze(include.mmat(reshape(O_0 ,1,nT,N),ConjXr3d));
            Mat_12= squeeze(include.mmat(reshape(O_D ,1,nT,N),ConjXr3d));

            PJfD20 = sum(Mat_11,2);
            PJfD21 = Mat_11*(0:N-1)';
            PJTh21 = sum(Mat_12,2);

            Eval_G_2= PJfD20'*PJfD20/N^2;
            Grad_wd_G_2 = -2/N^2*imag(PJfD21'*PJfD20);
            Grad_ws_G_2 = -2*gamma/N^2*imag(PJTh21'*PJfD20);

            Eval_H = Eval_P - Eval_G_2;
            Grad_wd_H = - Grad_wd_G_2;
            Grad_ws_H = Grad_P-Grad_ws_G_2;

            PJwD = Grad_wd_S_2 / Eval_H - Eval_S_2 * Grad_wd_H / Eval_H^2;
            PJwS = Grad_ws_S_2 / Eval_H - Eval_S_2 * Grad_ws_H / Eval_H^2;

            if (iter==1)
                l_new(1)=Eval_S_2/Eval_H;
            end

            grad= [PJwD;PJwS];
            % if (norm(grad)>1e-3)
            %     grad= grad/(500*norm(grad));
            % end
            grad= grad/(100*norm(grad));

            tt = [wdh(iter);wsh(iter)] + grad;

            iter=iter+1;
            wdh(iter) = tt(1);
            wsh(iter) = tt(2);

            V_0= exp(-1j*wdh(iter)*(0:N-1));
            W_0= exp(-1j*wsh(iter)*(0:(nR-1+gamma*(nT-1)))') * V_0;
            Mat_1= sum( W_0  .* MrConjX);
            PJfD0 = sum(Mat_1);
            Eval_S_2 =  PJfD0'*PJfD0/N^2;
            O_0= exp(-1j*wsh(iter)*gamma*(0:(nT-1))') * V_0;
            Mat_11= squeeze(include.mmat(reshape(O_0 ,1,nT,N),ConjXr3d));
            PJfD20 = sum(Mat_11,2);
            Eval_G_2= PJfD20'*PJfD20/N^2;

            [Eval_P]   =include.Eval_and_Grad_P(wsh(iter),Rxx,nT,gamma);

            l_new(iter) =  Eval_S_2/(Eval_P-Eval_G_2);
            t=1;a=0.1; b=0.3;
            Vect(iter)=1;
            while (l_new(iter)<l_new(iter-1)+a*t*(grad'*grad) && (t>=1e-15))
                Vect(iter)=Vect(iter)+1;
                t=t*b;
                tt = [wdh(iter-1);wsh(iter-1)] + t*grad;
                wdh(iter) = tt(1);
                wsh(iter) = tt(2);

                V_0= exp(-1j*wdh(iter)*(0:N-1));
                W_0= exp(-1j*wsh(iter)*(0:(nR-1+gamma*(nT-1)))') * V_0;
                Mat_1= sum( W_0  .* MrConjX);
                PJfD0 = sum(Mat_1);
                Eval_S_2 =  PJfD0'*PJfD0/N^2;
                O_0= exp(-1j*wsh(iter)*gamma*(0:(nT-1))') * V_0;
                Mat_11= squeeze(include.mmat(reshape(O_0 ,1,nT,N),ConjXr3d));
                PJfD20 = sum(Mat_11,2);
                Eval_G_2= PJfD20'*PJfD20/N^2;

                [Eval_P]   =include.Eval_and_Grad_P(wsh(iter),Rxx,nT,gamma);

                l_new(iter) =  Eval_S_2/(Eval_P-Eval_G_2);
            end
            gain=abs(l_new(end-1)-l_new(end));
            gain_wsh=abs(wsh(end-1)-wsh(end));
            gain_wdh=abs(wdh(end-1)-wdh(end));
        end
%         figure(14);hold on;plot(l_new')
%         figure(15);plot(1:iter,wD*ones(1,iter),'b');hold on;plot(wdh')
%         figure(16);plot(1:iter,wS*ones(1,iter),'b');hold on;plot(wsh')
        wsh=wsh(end);
        wdh=wdh(end);
%         keyboard
% iter
% gain
