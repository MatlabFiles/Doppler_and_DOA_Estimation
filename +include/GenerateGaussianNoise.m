function y =GenerateGaussianNoise(NRaw,NCol,Cov)
[U, D] = eig(Cov);
X=(randn(NRaw,NCol)+1j*randn(NRaw,NCol))/sqrt(2);
y = (U * D^(1/2) * X);

y=y/sqrt(trace(y*y')/NCol);