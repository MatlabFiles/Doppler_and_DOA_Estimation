function [wdh,wsh,l_new,iter,Vect]=Capon_IT_Newton(wdh,wsh,X,Mr,Ry,nT,nR,gamma,N,wdT,wsT)
        gain=1;
        iter=1;
        invRy=inv(Ry);
        Vect=[];
        Rxx=X*X'/N;
        TransX=reshape(X,1,nT,N);
        TransXHermMr=squeeze(include.mmat(TransX,permute(conj(Mr),[2 1 3])));
        while (gain>1e-5) && (iter<100)
            [Eval_ar_Ryinv_ar,Grad_ar_Ryinv_ar,Hess_ar_Ryinv_ar]=include.Eval_and_Grad_aRa(wsh(iter),invRy,nR);
            [Eval_P,Grad_P,Hess_P]                    =include.Eval_and_Grad_P  (wsh(iter),Rxx,nT,gamma);

            V_0= exp(1j*wdh(iter)*(0:N-1));
            W_0= exp(1j*wsh(iter)*(0:(nR-1+gamma*(nT-1)))')*V_0;
            W_D = 1j * diag(0:(nR-1+gamma*(nT-1)))*W_0;
            W_DD= 1j * diag(0:(nR-1+gamma*(nT-1)))*W_D;
            Mat_1= sum( W_0  .* TransXHermMr)/N;
            Mat_2= sum( W_D  .* TransXHermMr)/N;
            Mat_3= sum( W_DD .* TransXHermMr)/N;

            PJ0 = sum(Mat_1);
            PJwD1 = Mat_1*(1j*(0:N-1)');
            PJwD2 = Mat_1*(1j*(0:N-1)').^2;
            PJwS1 = sum(Mat_2);
            PJwS2 = sum(Mat_3);
            PJwSwD = Mat_2*(1j*(0:N-1)');
            
            Eval_S_2 =  PJ0'*PJ0;
            Grad_wd_S_2 = 2*real(PJwD1'*PJ0);
            Grad_ws_S_2 = 2*real(PJwS1'*PJ0);
            Hess_wd_S_2 = 2*(PJwD1'*PJwD1+real(PJwD2'*PJ0));
            Hess_ws_S_2 = 2*(PJwS1'*PJwS1+real(PJwS2'*PJ0));
            Hess_wd_ws_S_2 = 2*real( PJwD1' * PJwS1 + PJwSwD' * PJ0 );
            
            Eval_H = (Eval_P*Eval_ar_Ryinv_ar)^2;
            Grad_ws_H = 2*(Eval_P*Eval_ar_Ryinv_ar)*(Grad_P*Eval_ar_Ryinv_ar+Eval_P*Grad_ar_Ryinv_ar);
            Hess_ws_H = 2*(Grad_P*Eval_ar_Ryinv_ar+Eval_P*Grad_ar_Ryinv_ar)^2+2*(Eval_P*Eval_ar_Ryinv_ar)*(Hess_P*Eval_ar_Ryinv_ar+2*Grad_P*Grad_ar_Ryinv_ar+Eval_P*Hess_ar_Ryinv_ar);
            
            if (iter==1)
                l_new(1)=Eval_S_2/Eval_H;
            end
            
            G(1,1) = Grad_wd_S_2/Eval_H;
            G(2,1) = (Grad_ws_S_2*Eval_H-Grad_ws_H*Eval_S_2)/Eval_H^2;
            H(1,1) = Hess_wd_S_2/Eval_H;
            H(2,2) = ((Hess_ws_S_2*Eval_H-Hess_ws_H*Eval_S_2)*Eval_H-2*Grad_ws_H*(Grad_ws_S_2*Eval_H-Grad_ws_H*Eval_S_2))/Eval_H^3;
            H(1,2) = (Hess_wd_ws_S_2*Eval_H-Grad_wd_S_2*Grad_ws_H)/Eval_H^2;
            H(2,1) = H(1,2);
            newtonDir=- H\G;
            if ( G'*newtonDir<0)
                grad=G;
            else
                grad=newtonDir;
            end
            if (norm(grad)>1e-2)
                grad= grad*1e-2/norm(grad);
            end

            tt = [wdh(iter);wsh(iter)] + grad;
            
            iter=iter+1;
            wdh(iter) = tt(1);
            wsh(iter) = tt(2);
            
            V_0= exp(1j*wdh(iter)*(0:N-1));
            W_0= exp(1j*wsh(iter)*(0:(nR-1+gamma*(nT-1)))')*V_0;
            Mat_1= sum( W_0  .* TransXHermMr)/N;
            PJ0 = sum(Mat_1);
            
            [Eval_ar_Ryinv_ar]=include.Eval_and_Grad_aRa(wsh(iter),invRy,nR);
            [Eval_P]          =include.Eval_and_Grad_P  (wsh(iter),Rxx,nT,gamma);
            
            l_new(iter)=(PJ0'*PJ0)/(Eval_P*Eval_ar_Ryinv_ar)^2;
            t=1;a=0.4; b=0.5;
            Vect(iter)=1;
            while ((l_new(iter)<l_new(iter-1)+a*t*(G'*grad) ) && (t>=1e-3))
                Vect(iter)=Vect(iter)+1;
                t=t*b;
                tt = [wdh(iter-1);wsh(iter-1)] + t*grad;
                wdh(iter) = tt(1);
                wsh(iter) = tt(2);
                V_0= exp(1j*wdh(iter)*(0:N-1));
                W_0= exp(1j*wsh(iter)*(0:(nR-1+gamma*(nT-1)))')*V_0;
                Mat_1= sum( W_0  .* TransXHermMr)/N;
                PJ0 = sum(Mat_1);
                [Eval_ar_Ryinv_ar] =include.Eval_and_Grad_aRa(wsh(iter),invRy,nR);
                [Eval_P]           =include.Eval_and_Grad_P  (wsh(iter),Rxx,nT,gamma);
                l_new(iter)=(PJ0'*PJ0)/(Eval_P*Eval_ar_Ryinv_ar)^2;
            end
%             if (t<1e-3)
%                 display('t too small_stop iterative')
%                 break
%             end
            gain=abs(l_new(end-1)-l_new(end));
        end
%         figure(4);hold on;plot(l_new','r')
%         figure(2);plot(1:iter,wdT*ones(1,iter),'b');hold on;plot(wdh')
%         figure(3);plot(1:iter,wsT*ones(1,iter),'b');hold on;plot(wsh')
        wsh=wsh(end);
        wdh=wdh(end);