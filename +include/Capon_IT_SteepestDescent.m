function [wdh,wsh,l_new,iter,Vect]=Capon_IT_SteepestDescent(wdh,wsh,X,Mr,Ry,nT,nR,gamma,N,wdT,wsT)
        gain_wsh=1;
        gain_wdh=1;
        gain=1;
        iter=1;
        invRy=inv(Ry);
        Rxx=X*X'/N;
        Vect=[];
        ConjX=reshape(conj(X),nT,1,N);
        MrConjX=squeeze(include.mmat(Mr,ConjX));

%         while ((gain_wsh>1e-15)||(gain_wdh>1e-15)) && (iter<100)
        while (gain>1e-5) && (iter<100)
            [Eval_ar_Ryinv_ar,Grad_ar_Ryinv_ar]=include.Eval_and_Grad_aRa(wsh(iter),invRy,nR);
            [Eval_P,Grad_P]                    =include.Eval_and_Grad_P  (wsh(iter),Rxx,nT,gamma);

            V_0= exp(-1j*wdh(iter)*(0:N-1));
            W_0= exp(-1j*wsh(iter)*(0:(nR-1+gamma*(nT-1)))') * V_0;
            W_D= diag(0:(nR-1+gamma*(nT-1)))*W_0;
            Mat_1= sum( W_0  .* MrConjX);
            Mat_2= sum( W_D  .* MrConjX);
            
            PJfD1 = sum(Mat_1);
            PJfD2 = Mat_1*(0:N-1)';
            PJTh1 = sum(Mat_2);
            
            Eval_S_2 =  PJfD1'*PJfD1/N^2;
            Grad_wd_S_2 = -2/N^2*imag(PJfD2'*PJfD1);
            Grad_ws_S_2 =  -2/N^2*imag(PJTh1'*PJfD1);
            
            
            PJwD   = Grad_wd_S_2/(Eval_P*Eval_ar_Ryinv_ar)^2;
            PJwS =(Grad_ws_S_2*Eval_P*Eval_ar_Ryinv_ar-2*(Grad_ar_Ryinv_ar*Eval_P+Grad_P*Eval_ar_Ryinv_ar)*Eval_S_2)/(Eval_P*Eval_ar_Ryinv_ar)^3;
            
            if (iter==1)
                l_new(1)=Eval_S_2/(Eval_P*Eval_ar_Ryinv_ar)^2;
            end
            
            grad=[PJwD;PJwS];
            if (norm(grad)>1e-3)
                grad= grad/(500*norm(grad));
            end
            tt = [wdh(iter);wsh(iter)] + grad;
            
            iter=iter+1;
            wdh(iter) = tt(1);
            wsh(iter) = tt(2);
            
            V_0= exp(-1j*wdh(iter)*(0:N-1));
            W_0= exp(-1j*wsh(iter)*(0:(nR-1+gamma*(nT-1)))');
            Mat_1= sum(W_0 * V_0 .* MrConjX);
            PJfD1 = sum(Mat_1);
            
            [Eval_ar_Ryinv_ar]=include.Eval_and_Grad_aRa(wsh(iter),invRy,nR);
            [Eval_P]          =include.Eval_and_Grad_P  (wsh(iter),Rxx,nT,gamma);
            
            l_new(iter)=abs(PJfD1/N)^2/(Eval_P*Eval_ar_Ryinv_ar)^2;
            
            t=1;a=0.4; b=0.5;
            Vect(iter)=1;
            while ((l_new(iter)<l_new(iter-1)+a*t*([PJwD;PJwS]'*grad) ) && (t>=1e-15))
                Vect(iter)=Vect(iter)+1;
                t=t*b;
                tt = [wdh(iter-1);wsh(iter-1)] + t*grad;
                wdh(iter) = tt(1);
                wsh(iter) = tt(2);
                V_0= exp(-1j*wdh(iter)*(0:N-1));
                W_0= exp(-1j*wsh(iter)*(0:(nR-1+gamma*(nT-1)))');
                Mat_1= sum(W_0 * V_0 .* MrConjX);
                PJfD1 = sum(Mat_1);
                [Eval_ar_Ryinv_ar] =include.Eval_and_Grad_aRa(wsh(iter),invRy,nR);
                [Eval_P]           =include.Eval_and_Grad_P  (wsh(iter),Rxx,nT,gamma);
                l_new(iter)=abs(PJfD1/N)^2/(Eval_P*Eval_ar_Ryinv_ar)^2;
            end
%             if (t<1e-3)
%                 display('t too small_stop iterative')
%                 break
%             end
            
            gain=abs(l_new(end-1)-l_new(end));
            gain_wsh=abs(wsh(end-1)-wsh(end));
            gain_wdh=abs(wdh(end-1)-wdh(end));
        end
%         figure(4);hold on;plot(l_new','b')
%         figure(5);plot(1:iter,wdT*ones(1,iter),'b');hold on;plot(wdh')
%         figure(6);plot(1:iter,wsT*ones(1,iter),'b');hold on;plot(wsh')
        wsh=wsh(end);
        wdh=wdh(end);