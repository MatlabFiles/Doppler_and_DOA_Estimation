function Proposed_APES_OneTerm_Performance(SNR,BetaT,wsT,wdT_fixed,wsI,X,Rz,INR,nT,nR,gamma,N,Mc,NnS,NnD)
wwD = 2*pi*(-0.5:1/(NnD):(0.5-1/(NnD)));
wwS = 2*pi*(-0.5:1/(NnS):(0.5-1/(NnS)));
I=length(wsI);

Rxx=X*X'/N;
X3D=reshape(X,nT,1,N);

yI=0;
for i=1:I
    aT=exp(-1j*gamma*wsI(i)*(0:nT-1)');
    aR=exp( 1j*wsI(i).*(0:nR-1)');
    yI=yI+exp(1j*2*pi*rand)*aR*aT'*X;
end

mat=zeros(nR-1+gamma*(nT-1)+1,nT,N);
for n=1:N
    for p=1:nT
        mat(gamma*(p-1)+(1:nR),p,n)=1;
    end
end
mat=logical(mat);

P_Theta=zeros(NnD,NnS);
for f=1:NnS
    at=exp(-1j*gamma*wwS(f)*(0:nT-1)');
    P_Theta(:,f)=ones(NnD,1)*real(at'*Rxx*at);
end
alpha=7; p=round(alpha*NnS/360);

for j=1:length(SNR)
    display(strcat('SNR value',32, int2str(j), ' out of',32, int2str(length(SNR))))
    for k=1:Mc
        wdT=wdT_fixed+sqrt(0.001)*randn(1);
        aT=exp(-1j*gamma*wsT*(0:nT-1)');
        aR=exp( 1j*wsT*(0:nR-1)');
        yT=BetaT*(ones(nR,1)*exp(1j*wdT*(0:N-1))).*(aR*aT'*X);
        
        sigma_z=sqrt(trace(yT*yT'/N)./10.^(SNR(j)./10));
        z=sigma_z*include.GenerateGaussianNoise(nR,N,Rz);
        
        if I >0
            sigma_i=sqrt(10^(INR/10)*trace(z*z'/N)/trace(yI*yI'/N));
            yI=sigma_i*yI;
        end
        
        y=yT+yI+z;      
        Ry=y*y'/N;
        RyInv=inv(Ry);


        r= Ry\y;
        Mr_in=zeros(nR-1+gamma*(nT-1)+1,nT,N);
        Mr_in(mat)=kron(ones(nT,1),r);
        XR_in=squeeze(include.mmat(Mr_in,conj(X3D)));        

        Ay=fftshift(abs(fft2(XR_in.',NnD,NnS)/N).^2);
        
        U=chol(RyInv);
        r = U*y;
%         XR_u=zeros(gamma*(nT-1)+1,N,nR);
%         X_G=zeros(gamma*(nT-1)+1,N);
%         X_G(gamma*(0:nT-1)+1,:)=X;
%         for m=1:nR
%             XR_u(:,:,m) = (ones(gamma*(nT-1)+1,1)*r(m,:)).*conj(X_G);
%         end
%         XR_u=permute(XR_u,[2,1,3]);
%         Res_v=fft2(XR_u,NnD,NnS)/N;
%         Res=fftshift(sum(abs(Res_v).^2,3));


        arH_RyInv_ar=zeros(NnD,NnS);
        for f=1:NnS
            ar=exp( 1j*wwS(f)*(0:nR-1)');
            arH_RyInv_ar(:,f)=ones(NnD,1)*real(ar'*RyInv*ar);
        end

        Capon=Ay./(P_Theta.*arH_RyInv_ar).^2;
        [uD,vS]=find(Capon==max(max(Capon)));
        uD=uD(1); vS=vS(1);
        
        % Ignore maximums occuring around zero Doppler corresponding to the
        % interferers
        while(abs(wwD(uD))<0.25)
            Capon(:,max(vS-p,1):min(vS+p,NnS))=0;
            [uD,vS]=find(Capon==max(max(Capon)));
            uD=uD(1); vS=vS(1);
        end

        start=tic;
        [wdH,wsH,~,NumberOfIteration_SteepestDescent(k)]=include.Proposed_APES_SteepestDescent(wwD(uD(1)),wwS(vS(1)),X,Mr_in,r,nT,nR,gamma,N,wdT,wsT);
        ElapsedTime_SteepestDescent(k)=toc(start);
        
        start=tic;
        [wdH,wsH,~,NumberOfIterations_Newton(k)]=include.Proposed_APES_Newton(wwD(uD(1)),wwS(vS(1)),X,Mr_in,r,nT,nR,gamma,N,wdT,wsT);
        ElapsedTime_Newton(k)=toc(start);

        aT=exp(-1j*gamma*wsH.*(0:nT-1)');
        aR=exp( 1j*wsH.*(0:nR-1)');
        w=(Ry\aR)/real(aR'/Ry*aR);
        P=real(aT'*Rxx*aT);

        D = diag(exp(1j*wdH*(0:N-1)));
        BetaH= (w'*y*D'*X'*aT)/(P*N);

        SE_wd(j,k)  =  (wdH - wdT)^2;
        SE_ws(j,k)    =  (wsH - wsT)^2;
        SE_Beta(j,k)  =  abs(BetaH-BetaT)^2;
        SE_RBeta(j,k) =  (real(BetaH) - real(BetaT))^2;
        SE_IBeta(j,k) =  (imag(BetaH) - imag(BetaT))^2;
    end
    O_SE_wd(j,:)=sort(SE_wd(j,:));
    O_SE_ws(j,:)=sort(SE_ws(j,:));
    O_SE_Beta(j,:)=sort(SE_Beta(j,:));
    O_SE_RBeta(j,:)=sort(SE_RBeta(j,:));
    O_SE_IBeta(j,:)=sort(SE_IBeta(j,:));
    
    % Sort the MSE increasigly and remove 10% of the highest data 
    % this is done to discard erronous estimates in case the iterative
    % algorithm diverges from the local extremum
    Percent=0.9;
    MSE_wd(j)     = sum(O_SE_wd(j,1:Percent*Mc))/(Percent*Mc);
    MSE_ws(j)     = sum(O_SE_ws(j,1:Percent*Mc))/(Percent*Mc);
    MSE_Beta(j)   = sum(O_SE_Beta(j,1:Percent*Mc))/(Percent*Mc);
    MSE_RBeta(j)  = sum(O_SE_RBeta(j,1:Percent*Mc))/(Percent*Mc);
    MSE_IBeta(j)  = sum(O_SE_IBeta(j,1:Percent*Mc))/(Percent*Mc); 
    
    
end


[NI_SteepestDescent,X0_SteepestDescent]=hist(NumberOfIteration_SteepestDescent);
[NI_Newton,X0_Newton]=hist(NumberOfIterations_Newton);

[ET_SteepestDescent,X1_SteepestDescent]=hist(ElapsedTime_SteepestDescent);
[ET_Newton,X1_Newton]=hist(ElapsedTime_Newton);

figure;
bar(X0_SteepestDescent,NI_SteepestDescent)
hold on
bar(X0_Newton,NI_Newton,'r')
title('Camparison between the number of iterations required by both optimization algorithms')
xlabel('Number of Iterations')
ylabel('Number of Realizations')
legend('Steepest Descent', 'Newton Descent')


figure;
bar(X1_SteepestDescent,ET_SteepestDescent)
hold on
bar(X1_Newton,ET_Newton,'r')
title('Camparison between the elapsed time required by both optimization algorithms')
legend('Steepest Descent', 'Newton Descent')
ylabel('Number of Realizations')
xlabel('Elapsed Time (s)')


CRLB=include.CRLB(nT,nR,gamma,N,wsT,BetaT,SNR,X,Rz);
figure; hold on; grid on;
plot(SNR,10*log10(MSE_RBeta),'bv-')
plot(SNR,10*log10(MSE_IBeta),'k+-')
plot(SNR,10*log10(MSE_ws),'gp-')
plot(SNR,10*log10(MSE_wd),'ro-')

hold all;
plot(SNR,10*log10(CRLB(:,1)),'bv--')
plot(SNR,10*log10(CRLB(:,2)),'k+--')
plot(SNR,10*log10(CRLB(:,4)),'gp--')
plot(SNR,10*log10(CRLB(:,3)),'ro--')

title('Proposed APES')
legend_entry=legend('$\Re\{\hat{\beta}_t\}$','$\Im\{\hat{\beta}_t\}$','$\bar{\omega}_t$','$\omega_t$');
set(legend_entry,'Interpreter','latex');
ylabel('MSE/CRLB (dBs)')
xlabel('SNR')
set(gcf,'Color','w')