clc; clear;
close all;

%% Antenna Parameters
nT=10; nR=10; gamma=1;

% Number of Realizations
Mc = 1;
% Number of Samples
N = 32;

%% Target Parameters

ThetaT=[-10; 10; -20; 20];
wsT_all=2*pi*sind(ThetaT)/2;
wdT_all=[-1.2; 1.5; 0.6; -1.8];
BetaT_all= exp(1j*2*pi*rand(4,1));

T=1;
wsT=wsT_all(1:T);
wdT=wdT_all(1:T);
BetaT=BetaT_all(1:T);

%% Noise Power
SNR=0; %dB

%% Interferers Parameters
ThetaI=[20];
wsI=2*pi*sind(ThetaI)/2;
INR=0; %dB

%% FFT Length
NnD=1024;
NnS=1024;

%% Tansmit Signals
H = hadamard(N); X = H(1:nT,:);

%% Noise Covariance Matrix
% mat=(1:nR)'*ones(1,nR)-ones(nR,1)*(1:nR);
% Rz=0.9.^(abs(mat)) .* exp(1i*mat*pi/2);
Rz=eye(nR);

Capon_APES_Graphs(SNR,BetaT,wsT,wdT,wsI,X,Rz,INR,nT,nR,gamma,N,Mc,NnS,NnD)
pause(1) % To show the figures

% Load waveforms which maximize the beampattern in the region between
% [-30,30] degrees
load('Beamforming[-30,30]','X');
Rxx=X*X'/N;

Capon_APES_Graphs(SNR,BetaT,wsT,wdT,wsI,X,Rz,INR,nT,nR,gamma,N,Mc,NnS,NnD)
pause(1) % To show the figures


Mc = 100;
SNR=-30:5:10;
ThetaT=-5;
wsT=2*pi*sind(ThetaT)/2;
wdT=pi/2;
BetaT=(-1+2j)/sqrt(5);

ThetaI=[-30 50];
wsI=2*pi*sind(ThetaI)/2;
INR=0; %dB
N = 128;
H = hadamard(N); X = H(1:nT,:);

Capon_Performance(SNR,BetaT,wsT,wdT,wsI,X,Rz,INR,nT,nR,gamma,N,Mc,NnS,NnD)
pause(1) % To show the figures

Proposed_APES_OneTerm_Performance(SNR,BetaT,wsT,wdT,wsI,X,Rz,INR,nT,nR,gamma,N,Mc,NnS,NnD)
pause(1) % To show the figures

Conventional_APES_Performance(SNR,BetaT,wsT,wdT,wsI,X,Rz,INR,nT,nR,gamma,N,Mc,NnS,NnD)
pause(1) % To show the figures

Proposed_APES_GCF_Performance (SNR,BetaT,wsT,wdT,wsI,X,Rz,INR,nT,nR,gamma,N,Mc,NnS,NnD)
pause(1) % To show the figures
